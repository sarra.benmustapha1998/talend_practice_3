# talend_practice_3

Dans un contexte de production global, l'environnement de développement permet de peaufiner les différentes phases d'extraction et de transformation du flux de données, avec une surveillance et un reporting des journaux (logs) essentiels pour garantir la stabilité et les performances du système de gestion des flux de données.

Le processus d’ETL va permettre en soi, et de façon non exhaustive :

- De permettre l’extraction d’un ensemble de données (via du web scraping/appel d’API/table de saisie) vers un format exploitable (fichier texte délimité, format tabulaire).

- D’effectuer toutes les transformations relatives au data management telles que la concaténation de fichiers multiples, le traitement des doublons avec prise en compte des dates de dernières modifications.

- Le chargement des données data-managées dans un nouveau fichier avec un format spécifique (ou non) et permettre d'executer des fonctions de dataviz. 

Tout au long du processus, des fonctions de monitoring peuvent être mises en place afin de :

- Compter et effectuer une différence entre les entrées au départ, les entrées après chaque transformation et lors du chargement final.

- Obtenir les timestamps à chaque étape de transformation, afin de détecter une éventuelle anomalie de transformation (trop rapide/trop longue).

- Récupérer les logs d’erreur s'il y a lieu lors du processus d’ETL.

![](../png/02.png)